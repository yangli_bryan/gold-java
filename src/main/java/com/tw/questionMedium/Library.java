package com.tw.questionMedium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book ...books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     *   should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String ...tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        List<String> tagsToFind = Arrays.stream(tags).distinct().collect(Collectors.toList());
        List<Book> booksFound = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            Book bookFound = books.get(i);
            for (int j = 0; j < tagsToFind.size(); j++) {
                if (bookFound.getTags().contains(tagsToFind.get(j)) && !booksFound.contains(bookFound)) {
                    booksFound.add(bookFound);
                }
            }
        }
        Comparator<Book> compareByIsbn = Comparator.comparing(Book::getIsbn);
        booksFound.sort(compareByIsbn);
        return booksFound;
        // --end-->
    }

    // TODO:
    //   You can add additional methods here if you want.
    // <-start-

    // --end-->
}
